- `library.c` contains a simple library for adding single-precision floating
  point numbers in C.
- `library.h` contains headers for `library.c`.
- `main-generated.c` contains a simple program that adds two single-precision
  floating point numbers via `library.c` in C.
- `main-pie.s` contains a position-independent implementation of
  `main-generated.c` in x86_64 assembly using the SSE2 instruction set.
- `main.s` contains a position-dependent implementation of
  `main-generated.c` in x86_64 assembly using the SSE2 instruction set.
- `Makefile` documents the commands used for compiling C and linking assembly.
- `refs.url` contains references I found useful.
