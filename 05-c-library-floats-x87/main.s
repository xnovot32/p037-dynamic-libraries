  .section .rodata
printf_str:
  .string "%f + %f = %f\n"
float_1:
  .long 1069547520
float_2:
  .long 1075838976
  .text
  .globl main
main:
	pushq	%rbp
	movq	%rsp, %rbp
  
  movss (float_1), %xmm0
  movss (float_2), %xmm1
  call add # add(1.5, 2.5);
  mov $printf_str, %rdi
  cvtss2sd %xmm0, %xmm2 # upcast floats to doubles
  cvtss2sd (float_1), %xmm0
  cvtss2sd (float_2), %xmm1
  mov $2, %rax
  call printf     # printf("%f + %f = %f\n", 1.5, 2.5, add(1.5, 2.5));
  mov $0, %rax    # return 0;

  leave
  ret
