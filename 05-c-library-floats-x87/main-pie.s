  .section .rodata
printf_str:
  .string "%f + %f = %f\n"
float_1:
  .long 1069547520
float_2:
  .long 1075838976
  .text
  .globl main
main:
	pushq	%rbp
	movq	%rsp, %rbp
  
  movss float_1(%rip), %xmm0
  movss float_2(%rip), %xmm1
  call add # add(1.5, 2.5);
  leaq printf_str(%rip), %rdi
  cvtss2sd %xmm0, %xmm2 # upcast floats to doubles
  cvtss2sd float_1(%rip), %xmm0
  cvtss2sd float_2(%rip), %xmm1
  mov $2, %rax
  call printf@PLT # printf("%f + %f = %f\n", 1.5, 2.5, add(1.5, 2.5));
  mov $0, %rax    # return 0;

  leave
  ret
