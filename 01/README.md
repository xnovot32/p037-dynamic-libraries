- `hello.c` contains a simple hello world program in C.
- `Makefile` documents the command used for compiling `hello.c` to x86_64
  assembly for the 32bit ABI with disabled optimisations.
