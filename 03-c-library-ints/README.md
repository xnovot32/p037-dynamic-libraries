- `library.c` contains a simple library for adding integers in C.
- `library.h` contains headers for `library.c`.
- `main-generated.c` contains a simple program that adds two integers via
  `library.c` in C.
- `main-pie.s` contains a position-independent implementation of
  `main-generated.c` in x86_64 assembly.
- `main.s` contains a position-dependent implementation of
  `main-generated.c` in x86_64 assembly.
- `Makefile` documents the commands used for compiling C and linking assembly.
- `refs.url` contains references I found useful.
