  .section .rodata
printf_str_1:
  .string "%d + %d = %d\n"
printf_str_2:
  .string "%d * %d = %d\n"
  .text
  .globl main
main:
	pushq	%rbp
	movq	%rsp, %rbp
  
  mov $1, %rdi
  mov $2, %rsi
  call add        # add(1, 2);
  leaq printf_str_1(%rip), %rdi
  mov $1, %rsi
  mov $2, %rdx
  mov %rax, %rcx
  mov $0, %rax
  call printf@PLT # printf("%d + %d = %d\n", 1, 2, add(1, 2));

  mov $4, %rdi
  mov $5, %rsi
  call _Z3mulii   # mul(4, 5);
  leaq printf_str_2(%rip), %rdi
  mov $4, %rsi
  mov $5, %rdx
  mov %rax, %rcx
  mov $0, %rax
  call printf@PLT # printf("%d + %d = %d\n", 1, 2, add(1, 2));

  mov $0, %rax    # return 0;

  leave
  ret
