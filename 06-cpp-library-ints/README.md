- `library.cpp` contains a simple library for adding integers in C++.
- `library.hpp` contains headers for `library.cpp`.
- `main-generated.c` contains a simple program that adds two integers via
  `library.cpp` in C.
- `main-pie.s` contains a position-independent implementation of
  `main-generated.c` in x86_64 assembly.
- `main.s` contains a position-dependent implementation of
  `main-generated.c` in x86_64 assembly.
- `Makefile` documents the commands used for compiling C and linking assembly.
- `refs.url` contains references I found useful.
