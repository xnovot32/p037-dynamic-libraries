- `hello-generated.c` contains a simple hello world program in C.
- `hello-pie.s` contains a position-independent implementation of
  `hello-generated.c` in x86_64 assembly.
- `hello.s` contains a position-dependent implementation of `hello-generated.c`
  in x86_64 assembly.
- `Makefile` documents the commands used for compiling C and linking assembly.
- `refs.url` contains references I found useful.
