  .section .rodata
hello_str:
  .string "Hello world"
  .text
  .globl main
main:
  xor %rdi, %rdi
  mov $hello_str, %rdi
  call puts      # puts("Hello world");
  xor %rax, %rax # return 0;
  ret
