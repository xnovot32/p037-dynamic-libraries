  .section .rodata
hello_str:
  .string "Hello world"
  .text
  .globl main
main:
  xor %rdi, %rdi
  leaq hello_str(%rip), %rdi
  call puts@PLT  # puts("Hello world");
  xor %rax, %rax # return 0;
  ret
