This repository contains a number of examples detailing the use of dynamic
libraries from within the x86_64 assembly. `Makefile` documents the commands
required for running tests and removing auxiliary files from the
subdirectories.
