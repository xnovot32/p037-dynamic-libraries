.PHONY: all clean
all: clean

# Check that the resulting binaries produce the same output and remove all the
# generated files.
clean:
	@for i in */; do make -C "$$i" test clean; done
